const processPrefills = require('./process-prefills.js');
const realmCreateJob = require('./realm-create-job.js');

module.exports = async function realmCreateJobs({ strapiCompany: company, roles }){
    const prefillServices = strapi.services.prefill; 

    const unformattedJobPrefills = await prefillServices.find({ type: 'job' }); 

    const jobPrefills = processPrefills( unformattedJobPrefills );

    const rolesWithUid = roles.filter( role => role.uid );

    for( const role of rolesWithUid ){
        await realmCreateJob({ company, role, jobPrefills })
    }
}