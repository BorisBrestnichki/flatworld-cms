import React, { useState, useEffect } from 'react';
import authRequest from '../../lib/auth-request';
import styled from 'styled-components';

const Wrapper = styled.div`
	position: absolute; 
	height: 16px; 
	top: 5px; 
	right: 20px;
	cursor: pointer;

	& select { 
		position: absolute; 
		top: 0; 
		left: 0;
		width: 100%; 
		height: 100%; 
		opacity: 0; 
		cursor: pointer;
		font-size: 16px;
	}

	& option { 
		opacity: 1; 
		padding: 5px 20px;
	}

	& > div {
		font-size: 12px; 
		line-height: 16px;
		text-align: center; 
		pointer-events: none; 
	}
`;

function SelectHint ( props ) { 
	const { 
		name,
		type,
		fieldName,
		onChange
	} = props; 

	const target = fieldName.join( '.' );

	const [ hints, setHints ] = useState( [] );

	const [ loading, setLoading ] = useState( true );

	useEffect(() => { 
		( async () => { 
			try { 
				const data = await authRequest({ url: '/content-api/hints/search', params: { target } });

				setHints( data );
				setLoading( false );
			} catch ( e ) {
				console.log( e );
			}
		})()
	}, []);

	if( loading || hints.length === 0 ) return null;

	return (
		<Wrapper>
			<select
				onChange={ e => onChange({ target: { name, value: e.target.value, type } } )}
			>
				{ hints.map(( hint, index ) => {
					return (
						<option
							key={ index }
							value={ hint }
						> 
							{ truncateString( hint, 27 ) }
						</option>
					)
				})} 
			</select>
			<div> 
				HINTS
			</div>
		</Wrapper>
	)
}

export default SelectHint;

function truncateString(str, num) {
	if (str.length > num) {
	  return str.slice(0, num) + "...";
	} else {
	  return str;
	}
  }