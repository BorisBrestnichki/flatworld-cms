module.exports = ({ env }) => ({
  host: env( 'HOST', '0.0.0.0' ),
  port: env( 'PORT', 1337 ),
  admin: {
    host: '0.0.0.0',
    port: 8000,
    auth: {
      secret: env( 'STRAPI_JWT_SECRET', '849e09fa6bdba75588aa8c1561cdd038' ),
    },
  },
});
