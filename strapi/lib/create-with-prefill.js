const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

const processPrefills = require("./process-prefills");

module.exports = async function createWithPrefill({ ctx, service, model, prefillType }){ 
    const prefillsService = strapi.services.prefill; 

    const unformattedPrefills = await prefillsService.find({ type: prefillType });

    const prefills = processPrefills( unformattedPrefills ); 

    let entity;

    if (ctx.is('multipart')) {
        const { data: unformattedData, files } = parseMultipartData( ctx );

        const data = { ...unformattedData, ...prefills }; 

        try { 
            entity = await service.create( data, { files });
        } catch ( e ){
            console.log( e ); 

            entity = await service.create( unformattedData ); 
        }
    } else {
        const unformattedData = ctx.request.body; 

        const data = { ...unformattedData, ...prefills }

        try { 
            entity = await service.create( data );
        } catch ( e ){
            console.log( e ); 

            entity = await service.create( unformattedData ); 
        }
    }
    
    const sanitized = sanitizeEntity( entity, { model } );

    const { id } = sanitized; 

    const entityWithUrl = {
        ...sanitized, 
        adminUrl: `/admin/plugins/content-manager/collectionType/${ model.uid }/${ id }`
    }

    return entityWithUrl; 
}