const axios = require( 'axios' );
const slackPayload = require("./slack-payload");

module.exports = async function slackNotificaitons({ model, id, message }){ 
    // disable the notifications for first pull
    if( process.env.SLACK_NOTIFY !== 'true' ){
        console.log( 'Slack notifiactions disabled'); 
        return; 
    }

    const url = `${ process.env.PUBLIC_URL }/admin/plugins/content-manager/collectionType/${ model.uid }/${ id }`

    const payload = slackPayload({ message, url })  

    payload.channel = process.env.SLACK_CHANNEL; 

    const res = await axios.post( process.env.SLACK_URL, payload, {
        headers: { 
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${ process.env.SLACK_TOKEN }`
        }
    });

    const { data = {} } = res; 

    const { ok } = res; 

    if( !ok ){
        console.log( data );
    }
}