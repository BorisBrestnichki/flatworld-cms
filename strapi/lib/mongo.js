const { MongoClient } = require( 'mongodb' );

const client = new MongoClient( process.env.SOURCE_DATABASE_URI, { useNewUrlParser: true, useUnifiedTopology: true });

let connected

module.exports = {
    getClient: async () => {
        if( connected ){
            return client; 
        }

        await client.connect();

        connected = true;

        return client; 
    }
}