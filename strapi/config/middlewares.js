module.exports = {
    settings: {
      parser: {
        formLimit: '10mb', 
        jsonLimit: '10mb', 
        formidable: {
          maxFileSize: 100 * 1024 * 1024, // multipart data, modify here limit of uploaded file size
        },
      },
    },
  };