const formatSalaryNumber = ( job ) => { 
	const { salaryMin, salaryMax, salaryCurrency, salaryAggregation, } = job;

	const wtihAggregation = true;
	
	const currencySymbol = salaryCurrency === 'USD' ?
	  '$' : ( salaryCurrency === 'EUR' ? '€' : '£' )
  
	if( !salaryCurrency || !salaryAggregation ){
	  return 'Undisclosed';
	}
  
	if( !salaryMin && !salaryMax ){
	  return 'Undisclosed';
	}
  
	if( salaryMin && salaryMax ){
	  return `${ currencySymbol }${ numberWithCommas( salaryMin ) } to ${ currencySymbol }${ numberWithCommas( salaryMax ) } ${ salaryCurrency }${ wtihAggregation ? ` / ${ salaryAggregation }` : '' }`;
	}
  
	const salary = salaryMin || salaryMax; 
  
	return `${ currencySymbol }${ numberWithCommas( salary ) } ${ salaryCurrency }${ wtihAggregation ? ` / ${ salaryAggregation }` : '' }`;
  }
  
function numberWithCommas( x )  {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}	

module.exports = formatSalaryNumber;