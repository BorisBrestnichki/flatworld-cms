'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
	async find( ctx ){ 
		try { 
			const { request } = ctx; 

			const { query } = request; 

			const { target } = query; 

			if( !target ){
				return [];
			}

			const entity = await strapi.services.hints.findOne({ target });

			if( !entity ){
				return [];
			}

			const hints = entity.hints.map( hint => hint.value ); 

			return hints; 
		} catch ( e ) { 
			console.log( e );

			ctx.status = 500;
		}
	}
};
