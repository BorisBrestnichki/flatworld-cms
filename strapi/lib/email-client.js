const { createTransport } = require( 'nodemailer' );
const { google } = require( 'googleapis' );

const oAuth2Client = new google.auth.OAuth2(
    process.env.MAIL_OAUTH_CLIENT_ID,
    process.env.MAIL_OAUTH_CLIENT_SECRET,
    "https://developers.google.com/oauthplayground"
);

oAuth2Client.setCredentials({
    refresh_token: process.env.MAIL_OAUTH_REFRESH_TOKEN,
});

module.exports.getClient = async () => {
    
    const accessToken = await oAuth2Client.getAccessToken();
    
    const transporterConfig = {
        service: "gmail",
        auth: {
            type: "OAuth2",
            user: process.env.MAIL_USER,
            clientId: process.env.MAIL_OAUTH_CLIENT_ID,
            clientSecret: process.env.MAIL_OAUTH_CLIENT_SECRET,
            refreshToken: process.env.MAIL_OAUTH_REFRESH_TOKEN,
            accessToken,
        },
    };
    
    const transporter = createTransport( transporterConfig );

    await transporter.verify();

    return transporter; 
};
