const jsonwebtoken = require( 'jsonwebtoken' );

module.exports = async (ctx, next) => {
    if (!ctx.request || !ctx.request.header || !ctx.request.header['x-auth'] ){
        ctx.unauthorized(`You're not logged in!`);
        return; 
    }

    const header = ctx.request.header[ 'x-auth' ]; 

    const token = header.replace( 'Bearer ', '' ).replace(/"/g, '' );

    if( !token || typeof token !== 'string' ){
        ctx.unauthorized(`You're not logged in!`);
        return; 
    }

    try { 
        const decoded = await jsonwebtoken.verify( token, process.env.REALM_TOKEN_SECRET )

        return next();
    } catch ( e ){
        ctx.unauthorized(`You're not logged in!`);
    }    
}; 