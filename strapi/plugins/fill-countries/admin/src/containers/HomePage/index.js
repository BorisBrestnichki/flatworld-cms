import styled from 'styled-components';

import React, { memo, useEffect, useRef, useState } from 'react';
import authRequest from '../../lib/auth-request';

const Wrapper = styled.div`
  padding: 20px; 
`;

const Heading = styled.h1`
  font-size: 18px; 
  leadign: 24px; 
`;

const InputWrapper = styled.div`
  margin-top: 20px; 
`;

const Label = styled.label`
  display: block; 
  font-size: 16px; 
  line-height: 20px;
`;

const Select = styled.select`
  width: 200px;
  appearance: none; 
  border: #dedede;
  font-size: 16px; 
  leadign: 20px; 
  background: #fff; 
  border: 1px solid #dedede; 
  border-radius: 5px; 
  padding: 5px 10px;
  background-image: url("data:image/svg+xml;utf8,<svg fill='black' height='24' viewBox='0 0 24 24' width='24' xmlns='http://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
  background-repeat: no-repeat;
  background-position-x: 100%;
  background-position-y: 5px;
`

const Button = styled.button`
  padding: 5px 10px; 
  background: #333;
  color: #fff; 
  margin-top: 10px; 
  font-size: 16px; 
  line-height: 24px;
  
  &[disabled]{
    opacity: 0.6;
  }
`;

const Message = styled.div`
  margin-top: 20px;
  font-size: 16px; 
  line-height: 20px;
  color: #333;
`;

const HomePage = () => {
  const unloaded = useRef( false );
  const [ loading, setLoading ] = useState( true );
  const [ jobs, setJobs ] = useState( [] );
  const [ message, setMessage ] = useState( '' );

  const [ from, setFrom ] = useState( false );
  const [ to, setTo ] = useState( false )

  const valid = from && to ? true : false;

  useEffect(() => {
		( async () => {
			try { 
				const jobs = await authRequest({ url: '/content-api/jobs' });

				if( unloaded.current ) return; 

        const jobsWithSlugs = jobs.filter( job => job.slug );

				setLoading( false );

				setJobs( jobsWithSlugs );
			} catch ( e ){
				console.log( e ); 
	
				if( unloaded.current ) return; 
	
				setLoading( false );
				setMessage( 'Something went wrong. Please try again.' ); 
			}
		})();

		return () => unloaded.current = true; 
	}, []); 

  async function onSubmit(){
    try {
      setLoading( true );

      const data = await authRequest( { url: '/fill-countries/fill', options: { 
        method: 'post', 
        headers: { 
          'Content-Type' : 'application/json'
        },
        body: JSON.stringify({ from, to })
      } } ); 

      if( unloaded.current ) return; 

      if( data.error ){
        setMessage( data.error ); 
        return;
      }

      setMessage( 'Done' );
      setLoading( false );
    } catch ( e ){
      console.log( e );

      if( unloaded.current ) return;

      setMessage( 'There was an error. Please try agian' ); 
      setLoading( false );
    }
  }

  return (
    <Wrapper>
      <Heading>Transfer Countries</Heading>
      <InputWrapper> 
        <Label>From</Label>
        <Select
          value={ from }
          onChange={ e => { 
            setMessage( null );
            setFrom( e.target.value );
          }}
          disabled={ loading }
        >
          <option
            value={ false } 
            disabled={ true }
          > 
            Choose Job
          </option>
          { jobs.map( job => {
            const  { _id, slug } = job; 

            return (
              <option
                key={ _id }
                value={ _id }
              >
                { slug }
              </option>
            )
          })}
        </Select>
      </InputWrapper>
      <InputWrapper>
        <Label>To:</Label>
        <Select
          value={ to }
          onChange={ e => { 
            setMessage( null );
            setTo( e.target.value );
          }}
          disabled={ loading }
        >
          <option
            value={ false }
            disabled={ true }
          > 
            Choose Job
          </option>
          { jobs.map( job => {
            const  { _id, slug } = job; 

            return (
              <option
                key={ _id }
                value={ _id }
              >
                { slug }
              </option>
            )
          })}
        </Select>
      </InputWrapper>
      <Button
        onClick={ onSubmit }
        disabled={ !valid || loading }
      >
        Transfer
      </Button>
      { message && 
        <Message>
          { message }
        </Message>
      }
    </Wrapper>
  );
};

export default memo(HomePage);
