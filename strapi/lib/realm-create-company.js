const processPrefills = require('./process-prefills.js');
const slackPost = require('./slack-post.js');

module.exports = async function realmCreateCompany({ uid, name, website }){
    if( !uid ) return; 

    const companyServices = strapi.services.company; 
    const companyModel = strapi.models.company; 
    const prefillServices = strapi.services.prefill; 

    const strapiCompany = await companyServices.findOne({ uid }); 

    if( strapiCompany ){
        return strapiCompany; 
    }
    
    const unformattedPrefills = await prefillServices.find({ type: 'company' }); 

    const prefills = processPrefills( unformattedPrefills );

    const company = await companyServices.create({ uid, name, website, ...prefills })

    const { _id: id } = company; 

    await slackPost({ id, model: companyModel, message: `New CMS Company - ${ name }` }); 

    return company;
}