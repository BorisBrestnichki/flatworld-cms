import React, { memo, useState, useEffect, useRef } from 'react';

import styled from 'styled-components';

import authRequest from '../../lib/auth-request';

const Wrapper = styled.div`
	font-size: 16px; 
	padding: 20px; ;
`

const Heading = styled.h1`
  margin-bottom: 20px; 
`

const Select = styled.select`
	appearance: none;
	padding: 5px 20px; 
	background-color: #fff; 
	border: 1px solid #E3E9F3;
	background-image: url(data:image/svg+xml;base64,PHN2ZyBmaWxsPSdibGFjaycgaGVpZ2h0PScyNCcgdmlld0JveD0nMCAwIDI0IDI0JyB3aWR0aD0nMjQnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+PHBhdGggZD0nTTcgMTBsNSA1IDUtNXonLz48cGF0aCBkPSdNMCAwaDI0djI0SDB6JyBmaWxsPSdub25lJy8+PC9zdmc+);
	background-repeat: no-repeat;
	background-position-x: 100%;
	background-position-y: 5px;
`

const ButtonWrapper = styled.div`
	margin: 20px 0;
`

const Button = styled.button`
	padding: 5px 10px; 
	background-color: #333; 
	color: #fff; 

	[disabled]{
		opacity: 0.6
	}
`

const Message = styled.div`
	margin: 20px 0; 
`

const HomePage = () => {
	const [ loading, setLoading ] = useState( true ); 

	const [ items, setItems ] = useState([]); 

	const [ selected, setSelected ] = useState( 'false' ); 

	const [ message, setMessage ] = useState( false )

	const unloaded = useRef( false ); 

	useEffect(() => {
		( async () => {
			try { 
				const types = await authRequest({ url: '/content-manager/content-types' });

				if( types.error ) throw new Error( res.message );

				if( unloaded.current ) return; 

				const activeItems = types.data.filter( item => item.isDisplayed && item.kind === 'collectionType' );

				setLoading( false );
				setItems( activeItems );
			} catch ( e ){
				console.log( e ); 
	
				if( unloaded.current ) return; 
	
				setLoading( false );
				setMessage( 'Something went wrong. Please try again.' ); 
			}
		})();

		return () => unloaded.current = true; 
	}, []); 

	function onChange( e ){ 
		const value = e.target.value; 

		setSelected( value )
	}

	function onSubmit(){
		( async () => {
			try {
				setLoading( true );

				const res = await authRequest({ url: '/create-and-prepopulate/create-content', options: { 
					method: 'post',
					headers: { 
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({ 
						type: selected
					}),
				}});

				if( res.error ) throw new Error( res.message );

				if( unloaded.current ) return; 

				setLoading( false )

				const { adminUrl } = res ;

				setMessage( `Generated. Edit from <a href="${ adminUrl }">here.</a>`)
			} catch ( e ){ 
				console.log( e );
	
				if( unloaded.current ) return; 
	
				setLoading( false );
				setMessage( 'Something went wrong. Please try again.' ); 
			}
		})();

		return () => unloaded.current = true; 
	}
	

	return (
		<Wrapper
			style={ loading ? { opacity: 0.6 } : null }
		>
			<Heading> 
				Create entry in: 
			</Heading>
			<ButtonWrapper>
				<Select
					value={ selected }
					onChange={ onChange }
				>
					<option value="false" disabled > Select Content Type </option>
					{ items.map( item => { 
						const { uid, info } = item; 

						const type = uid.split( '.' ).pop();

						const { label } = info; 

						return (
							<option 
								key={ uid }
								value={ type }
							>
								{ label }
							</option>
						);
					}) }
				</Select>
			</ButtonWrapper> 
			<ButtonWrapper>
				<Button
					disabled={ loading || selected === 'false' }
					style={ loading || selected === 'false' ? { opacity: 0.6 } : null }
					onClick={ onSubmit }
				>
					Create
				</Button>
				{ !message ? null : 
					<Message
						dangerouslySetInnerHTML={{ __html: message }}
					/>
				}
			</ButtonWrapper>
		</Wrapper> 
	);
};

export default memo( HomePage );
