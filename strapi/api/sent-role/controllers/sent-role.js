'use strict';

const mailClient = require ( '../../../lib/email-client' );
const formatSalaryNumber = require('../../../lib/format-salary');
const testEmail = require('../../../lib/test-email');

const slugRegex = /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/;

module.exports = {
    async create( ctx ){ 
        try { 
            const { request } = ctx; 

            const { body } = request;

            const { email, slug, dummy } = body; 

            if( !testEmail( email ) || !slugRegex.test( slug )){
                ctx.status = 400; 
                return { status: 'bad request' };
            }

            const role = await strapi.services['send-role-mail'].find( {} ); 

            const job = await strapi.services[ 'job' ].findOne({ slug }); 

            if( !job ){
                ctx.status = 400; 
                return { status: 'bad request' };
            }

            const { subject = '', body : mailBody  = '' } = role; 

			if( !dummy ){
				await strapi.services[ 'sent-role' ].create({ email, slug });
			}

			const { name, company, mustHaveTechItems, hasEquity, applyUrl } = job; 

			const { name: companyName, secondary } = company || {}; 

			const combinedName = `${ name || '' }${ companyName ? ' at ' : '' }${ companyName ? companyName : '' }`
			const salary = formatSalaryNumber( job );
			const combinedTechs = mustHaveTechItems?.filter( item => item.name )?.map( item => item.name )?.join( ', ' );
			const benefits = secondary
								?.filter( item => item.heading )
								?.find( item => item.heading.toLowerCase().indexOf( 'benefits' ) !== -1 )?.content;

            let html = mailBody
						.replace( '$name', combinedName ? `<strong>Role:</strong> ${ combinedName }` : '' )
						.replace( '$salary', `<strong>Salary</strong>: ${ salary }${ hasEquity ? ' + equity' : '' }` )


			if( combinedTechs ){
				html = html.replace( '$techs', `<strong>Tech stack:</strong> ${ combinedTechs }` );
			} else {
				html = html.replace( /(?:<p>)?\s*?\$techs\s*?(?:<\/p>)?/, `` );
			}

			if( benefits ){
				html = html.replace( '$benefits', `<strong>Benefits:</strong><br />${ benefits }` )
			} else { 
				html = html.replace( /(?:<p>)?\s*?\$benefits\s*?(?:<\/p>)?/, '' )
				
			}	
			
			if( applyUrl ){
				html = html.replace( '$url', `If you’re interested in applying, here’s a special link to jump straight to our initial tech skills quiz!<br/><br/><a href="${ applyUrl }">Click here to apply for this role</a>` )
			} else { 
				html = html.replace( '$url', `<a href="${ process.env.WEBSITE_PUBLIC_URL }/job/${ slug }/">Click here to view the role on our website.</a>` );
			}

            const client = await mailClient.getClient();
            
            await client.sendMail({
                from: {
                    name: "Tedi from Flatworld",
                    address: process.env.MAIL_USER,
                },
                to: email,
                subject,
                html,
            });

            ctx.status = 200; 
        
            return { status: 'ok' };
        } catch ( e ) { 
            console.log( e );

            ctx.status = 500; 

            return { status: 'server error' }
        }
    }
};
