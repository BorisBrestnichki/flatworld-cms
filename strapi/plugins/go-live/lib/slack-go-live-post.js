const axios = require( 'axios' );
const slackPayload = require("./slack-go-live-payload");

module.exports = async function slackGoLivePost({ message }){ 
    // disable the notifications for first pull
    if( process.env.SLACK_NOTIFY !== 'true' ){
        console.log( 'Slack notifiactions disabled'); 
        return; 
    }

    const payload = slackPayload({ message })  

    payload.channel = process.env.SLACK_BACKOFFICE_CHANNEL; 

    const res = await axios.post( process.env.SLACK_URL, payload, {
        headers: { 
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${ process.env.SLACK_TOKEN }`
        }
    });

    const { data = {} } = res; 

    const { ok } = res; 

    if( !ok ){
        console.log( data );
    }
}