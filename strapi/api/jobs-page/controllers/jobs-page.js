'use strict';
const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
	async findActiveState( ctx ) { 
		const jobsPage = await strapi.services[ 'jobs-page' ].find({});

        jobsPage.jobs = ( jobsPage.jobs || [] ).filter( job => job.activeState === 'active' );

        return sanitizeEntity( jobsPage, { model: strapi.models[ 'jobs-page' ] } );
	},
	async findActiveAndStagingState( ctx ) { 
		const jobsPage = await strapi.services[ 'jobs-page' ].find({});

        jobsPage.jobs = ( jobsPage.jobs || [] ).filter( job => job.activeState === 'active' || job.activeState === 'staging' );

        return sanitizeEntity( jobsPage, { model: strapi.models[ 'jobs-page' ] } );
	}
};
