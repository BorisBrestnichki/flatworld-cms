'use strict';

module.exports = {
	createContent: async ctx => {
		const { request } = ctx; 
		const { body } = request; 

		const { type } = body;

		const entity = await strapi.controllers[ type ].create( ctx )

		return entity; 
	}
};
