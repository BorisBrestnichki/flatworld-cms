module.exports = function slackGoLivePayload({ message }){
    const blocks = []; 

    blocks.push({
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": message
        },
    });

    return { blocks }; 
}