'use strict';

const mailClient = require ( '../../../lib/email-client' );
const testEmail = require("../../../lib/test-email");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
*/

const nameRegex = /^(?:\p{L}|[ ,.'-])+$/iu;
const linkRegex = new RegExp( '^\s*?((?:http:\/\/)|(?:https:\/\/))?(www\.)?((?:github\.com)|(?:linkedin\.com))([\/a-zA-Z0-9\._-]*)\s*?$', 'i' );
const timezoneRegex = /^\s*GMT[+|-]\d{1,2}(?:\.\d{1})?\s*$/

module.exports = {
    async create( ctx ){ 
        try { 
            const { request } = ctx; 

            const { body } = request;

            const { firstName, lastName, email, url, message, timezone } = body; 

            if( 
                !testEmail( email ) || 
                !nameRegex.test( firstName ) || 
                !nameRegex.test( lastName ) || 
                ( url && !linkRegex.test( url )) ||
				( timezoneRegex !== '' && !timezoneRegex.test( timezone ) )
            ) { 
                ctx.status = 400; 

				try { 
					console.log( 'Unformatted request:', JSON.stringify( body, null, 4 ) ); 
				} catch { 
					console.log( 'Unformatted request. Unable to stringify' );
				}

				console.log( 'email: ', testEmail( email ) );
				console.log( 'first name: ', nameRegex.test( firstName ) );
				console.log( 'last name: ', nameRegex.test( lastName ) );
				console.log( 'url:', !url || linkRegex.test( url ) );
				console.log( 'timezone:', timezoneRegex.test( timezone ) )

                return { status: 'unformatted request'};
            }

            const safeMessage = message.replace( /[^a-z0-9áéíóúñü \.,_\-:\/!?]/gim, '' ); 

            const client = await mailClient.getClient();

            const internal = await client.sendMail({
                from: {
                    name: `CMS Request from ${ email }`,
                    address: process.env.MAIL_USER,
                },
                to: process.env.MAIL_REQUEST_MAIL,
				replyTo: email,
                subject: `New Contact Form Request from ${ email }` ,
                html: `
                    <h1>Request</h1>
                    <h2>Name</h2>
                    <p>${ firstName } ${ lastName }</p>
                    <h2>Email</h2>
                    <p>${ email }</p>
                    ${ url ? `<h2>LinkedIn / Github</h2>
                    <p>${ url }</p>` : '' } 
                    <h2>Message</h2>
                    <p>${ safeMessage }</p>
					<h2>Timezone</h2>
					<p>${ timezone || 'Not available' }</p>
                `,
            });

			const response = await client.sendMail({
                from: {
                    name: `Tedi from Flatworld`,
                    address: process.env.MAIL_USER,
                },
				cc: [ process.env.MAIL_REQUEST_MAIL ],
                to: email,
				replyTo: process.env.MAIL_REQUEST_MAIL,
                subject: `Thank you for your interest in our positions.` ,
                html: `
					<p>
						Hi ${ firstName },<br />
						Thank you for sending in your request.<br />
						Please review our <a href="https://intercom.help/flatworldco/en/collections/2688900-flatworld-faq-s">FAQ's page</a> and see if any of the articles may answer your question.
					</p>
					<p>
						If you did not find a role that matches your skills on our <a href="https://flatworld.co/jobs">jobs page</a> unfortunately we do not have a position available for you at the moment. <br />
						We have recorded your request and will reach out to you in the future when a role becomes available.
					</p>
					<p>
						Thank you and good luck on your job search,<br />
						The Flatworld Team.
					</p>
                `,
            });

			const { accepted: acceptedInternal = [] } = internal || {}; 

			if( acceptedInternal.length !== 1 ){
				console.log( 'Internal email not delivered' )
			}

			const { accepted : acceptedResponse = [], rejected: rejectedResponse = [] } = response || {}; 

			if( acceptedResponse.length !== 2 ){
				console.log( `Unable to deliver autoreponse for ${ email }. Delivered: ${ acceptedResponse.join( ',' ) }. Rejected: ${ rejectedResponse.join( ',' ) }` ); 
			}

			await strapi.services.request.create({ firstName, lastName, email, url, message, timezone });

            ctx.status = 200; 

            return { status: 'ok' };
        } catch ( e ) { 
            console.log( e ) 

            ctx.status = 500; 

            return { status: 'server error' }
        }
    }
};
