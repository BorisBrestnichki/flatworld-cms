module.exports = function processPrefills( prefills ){    
    const formatted = prefills.reduce(( acc, item ) => { 
        const { field, items: unformattedItems } = item; 

        const items = unformattedItems.map( unformattedItem => { 
            const { _id, id, ...item } = unformattedItem; 

            return item; 
        });

        const singleItem = items.find( item => item.__component.indexOf( 'single-prefill' ) !== -1 ); 

        if( singleItem ){
            return {
                ...acc, 
                [field]: singleItem.value,
            }
        }

        return ({
            ...acc, 
            [field]: items
        }); 
    }, {});

    return formatted; 
}