export default async function authRequest({ url, params = {}, options = {} }){ 
    const token = localStorage.getItem("jwtToken") || sessionStorage.getItem("jwtToken");

    if( !token ) throw new Error( 'You are not logged in' ); 

    const urlParams = new URLSearchParams( params );

    const { headers = {} } = options;

    const response = await fetch(
        `${ strapi.backendURL }${url}${ urlParams.toString() !== '' ? `?${ urlParams.toString() }` : '' }`, 
        {
            ...options,
            headers: {
                ...headers, 
                Authorization: `Bearer ${ token.replace( /"/g, '' ).replace( /'/g, '' )}`,
            },
        }
    );

    const data = await response.json();

    return data; 
}