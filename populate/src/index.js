import mongodb from 'mongodb'
import strapiHealthcheck from './healthcheck.js';
import updateCompany from './updateCompany.js';

const strapiReady = await strapiHealthcheck(); 

if( !strapiReady ) { 
    throw new Error( 'No strapi instance' )
}

const { MongoClient } = mongodb; 

const client = new MongoClient( process.env.SOURCE_DATABASE_URI, { useNewUrlParser: true, useUnifiedTopology: true });

await client.connect();

const companiesCollection = client.db( 'companiesDB' ).collection( 'companiesCollection' );

const companies = await companiesCollection.find({}).toArray();

for( const company of companies ){ 
    await updateCompany( company );  
}

