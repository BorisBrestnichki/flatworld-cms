const slackPost = require("./slack-post");
const { buildQuery } = require('strapi-utils');

module.exports = async function realmCreateJob({ company, role, jobPrefills }){   
    const jobServices = strapi.services.job; 
    const jobModel = strapi.models.job; 
    const jobsPageServices = strapi.services[ 'jobs-page' ];
    
    const { uid, roleName: name, technicalItems = {}, active } = role; 

    const { mustHaveTech : sourceMustHaveTech = [], niceToHaveTech : sourceNiceToHaveTech = [] } = technicalItems;   

    const strapiRole = await jobServices.findOne({ uid }); 

    if( strapiRole && !active ){ 
        const { _id } = strapiRole; 

        await jobServices.update({ _id }, { activeState: 'removed' }); 

        return;
    }

    if( strapiRole ) return; 

    const { _id: comapnyId, name: companyName } = company; 
    
    const sourceMustHaveIds = sourceMustHaveTech.map( tech => tech.uid ).filter( uid => uid );
    const sourceNiceToHaveIds = sourceNiceToHaveTech.map( tech => tech.uid ).filter( uid => uid );

    const mustHaveTechItems = await buildQuery({
        model: strapi.models[ 'tech-item' ],
        filters: { where: [ { field: 'uid', operator: 'in', value: sourceMustHaveIds } ] } ,
    });

    const niceToHaveTechItems = await buildQuery({
        model: strapi.models[ 'tech-item' ],
        filters: { where: [ { field: 'uid', operator: 'in', value: sourceNiceToHaveIds } ] } ,
    });


    const mustHaveTechItemsIds = mustHaveTechItems.map( item => item._id );
    const niceToHaveTechItemsIds = niceToHaveTechItems.map( item => item._id );

    const newRole = await jobServices.create({ 
        uid,
		activeState: 'removed',
        name, 
        mustHaveTechItems: mustHaveTechItemsIds, 
        niceToHaveTechItems: niceToHaveTechItemsIds,
        company: comapnyId, 
        ...jobPrefills,
    });

    const { _id: id, name: newRoleName } = newRole; 

    const jobsPage = await jobsPageServices.find({});

    const { jobs = [] } = jobsPage || {};

    const newJobs = [ id, ...jobs ]; 

    jobsPageServices.createOrUpdate({ jobs: newJobs })

    const message = `New job for ${ companyName } - ${ newRoleName }`; 

    await slackPost({ id, model: jobModel, message }); 
}