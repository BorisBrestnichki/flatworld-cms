'use strict';

const { sanitizeEntity } = require('strapi-utils');
const createWithPrefill = require("../../../lib/create-with-prefill");
const { buildQuery } = require('strapi-utils');

module.exports = {
	findActiveState : async ctx => { 
        const unformattedJobs = await strapi.services.job.find({ activeState: 'active', _limit: 300 }); 

        const jobs = unformattedJobs.map( job => sanitizeEntity( job, { model : strapi.models.job }));

        return jobs; 
    },
	findActiveAndStagingState : async ctx => { 
        const unformattedJobs = await strapi.services.job.find({ activeState: { $in: [ 'active', 'staging' ] }, _limit: 300 }); 

        const jobs = unformattedJobs.map( job => sanitizeEntity( job, { model : strapi.models.job }));

        return jobs; 
    },
	findAll: async ctx => { 
        const unformattedJobs = await strapi.services.job.find({ _limit: 300 }); 

        const jobs = unformattedJobs.map( job => sanitizeEntity( job, { model : strapi.models.job }));

        return jobs; 
    },
    create: ctx => createWithPrefill({ 
        ctx, 
        service: strapi.services.job, 
        model: strapi.models.job,
        prefillType: 'job' 
    }),
    async countries( ctx ){
        const { request } = ctx; 

        const { body: { countries, slug } } = request; 

        if( !slug || !countries || countries.length === 0 ){
            return { error: 'no params' };
        }

        const savedCountries = await strapi.services[ 'country' ].find( { _limit: 300 } );

        const invalidCountries = countries.filter( country => !savedCountries.find( savedCountry => savedCountry.name === country ) );

        if( invalidCountries.length > 0 ){
            return { 
                invalidCountries
            }
        }

        const countryEntries = await buildQuery({
            model: strapi.models[ 'country' ],
            filters: { where: [ { field: 'name', operator: 'in', value: countries } ] } ,
        });

        const job = await strapi.services[ 'job' ].findOne({ slug });

        if( !job ) return { error: 'no job' };

        const { _id } = job; 

        await strapi.services[ 'job' ].update({ _id }, { countries: countryEntries })

        return { staus: 'done' }
    },
    async activeJobs( ctx ){
        try { 
            const jobsPage = await strapi.services['jobs-page'].find({});

			const { jobs } = jobsPage; 

            const validJobs = ( jobs || [] ).filter( job => job.activeState === 'active' && job.company && job.slug && job.name && job.applyUrl );

            const formatted = validJobs.map( job => {
                const { name, slug, company, applyUrl } = job; 

                const { name : companyName } = company;

                const url = `${ process.env.WEBSITE_PUBLIC_URL }/job/${ slug }/`;

                return { 
                    name, 
                    url,
                    companyName, 
                    applyUrl
                }
            })

            return formatted;
        } catch ( e ) { 
            console.log( e ); 

            ctx.status = 500; 

            return [];
        }
    }
};
