import fetch from 'node-fetch';

export default function updateCompany( company ){
    return fetch( 'http://strapi:1337/insecure-content-api/companies/realm', { 
        method: 'post',
        headers: { 
            'Content-Type': 'application/json',  
        },
        body: JSON.stringify( company ),       
    })
}