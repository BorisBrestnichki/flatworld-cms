'use strict';
const createWithPrefill = require("../../../lib/create-with-prefill");
const realmCreateCompany = require('../../../lib/realm-create-company');
const realmCreateJobs = require("../../../lib/realm-create-jobs");
const realmCreateTechs = require("../../../lib/realm-create-techs");
const { buildQuery } = require('strapi-utils');

module.exports = {    
    create: ctx => createWithPrefill({ 
        ctx, 
        service: strapi.services.company, 
        model: strapi.models.company,
        prefillType: 'company' 
    }), 
    async realmCreate( ctx ){ 
        try {
            const { request } = ctx; 
            const { body } = request; 

            const { uid, companyName: name, roles = [], website = '', dummy } = body; 

            if( dummy ){
                ctx.status = 200;  
                return;
            }

            if( !uid ){
                ctx.status = 400; 
                return; 
            } 

            const mongo = require( '../../../lib/mongo.js' );

            const client = await mongo.getClient(); 

            const techMetas = await client.db( 'techDB' ).collection( 'techMetadataCollection' ).find({}).toArray(); 

            await realmCreateTechs( body, techMetas );

            const strapiCompany = await realmCreateCompany({ uid, name, website });

            await realmCreateJobs({ strapiCompany, roles });            

            ctx.status = 200; 
        } catch ( e ){
            console.log( e ); 

            ctx.status = 500;
        }
    },
    async searchByUtm( ctx ){ 
        try { 
            const { request } = ctx; 
            const { body } = request; 

            const { utmCampaign } = body; 

            if( !utmCampaign ){
                ctx.status = 400; 
                return; 
            } 

            const search = `utm_campaign=${ utmCampaign }`;

            const rawJobs = await buildQuery({
                model: strapi.models[ 'job' ],
                filters: { 
                    where: [ { 
                        field: 'applyUrl', 
                        operator: 'contains', 
                        value: search
                    } ] 
                } ,
            });

            const regex = new RegExp( `${ search }(?:$|\s|&)`, 'i');

            const jobs = rawJobs.filter( job => regex.test( job.applyUrl ) )

            if( jobs.length === 0 ){
                return { 
                    error : 'no job with that such apply url'
                }
            }

            if( jobs.length > 1 ){
                return { 
                    error : 'more than one job with such apply url'
                }
            }

            const job = jobs[ 0 ]

            const { slug } = job; 

            if( !slug ){
                return { 
                    error : 'job does not have slug'
                }
            }

            const companyId = job.company; 

            if( !companyId ){ 
                return { 
                    error : 'this job is not conected to company'
                }
            }

            const company = await strapi.services.company.findOne({ _id : companyId });

            const { name } = company; 

            return { 
               link: `${ process.env.WEBSITE_PUBLIC_URL }/job/${ slug }/`,
               company: name || ''
            }
        } catch ( e ) { 
            console.log( e );

            ctx.status = 500; 

            return [];
        }
    }
};
