module.exports = function slackPayload({ message, url }){
    const blocks = []; 

    blocks.push({
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": `${ message }. Edit here: ${ url }`,
        },
    });

    return { blocks }; 
}