'use strict';
const axios = require( 'axios' );
const jwt = require( 'jsonwebtoken' );
const slackGoLivePost = require('../lib/slack-go-live-post');

function checkProperty( jobs, key, ctx ){
	const jobsWithoutPropery = jobs.filter( job => 
		( job.activeState === 'active' || job.activeState === 'staging' ) && !job[key]
	);

	if( jobsWithoutPropery.length > 0 ){
		ctx.send({ 
			ok: false, 
			message: `There\'s jobs without "${ key }": ${ jobsWithoutPropery.map( job => job.slug || job.id ).join( ', ') }`
		});

		return false; 
	}

	return true; 
}

module.exports = {
	healthcheck: async ctx => {
		ctx.send({
			message: 'ok'
		});
	},
	token: async ctx => {
		try { 
			const exp = parseInt( new Date( new Date().setDate( new Date().getDate() + 1 )).getTime() / 1000 );

			const token = jwt.sign({
				exp
			}, process.env.PREVIEW_SECRET ); 

			ctx.cookies.set( 'token', token, { path: '/', domain: process.env.DOMAIN, overwrite: true, sameSite: 'Strict', httpOnly: false });

			ctx.send({ redirect: process.env.PREVIEW_URL })
		} catch ( e ){ 
			console.log( e );

			ctx.status = 500; 
		}
	},
	triggerBuild: async ctx => { 
		try { 
			const { request } = ctx; 

			const { body } = request; 

			const { url, title } = body; 

			if( !url ){
				ctx.status = 400; 
				return; 
			}

			const jobsPage = await strapi.services[ 'jobs-page' ].find({})

			const { jobs } = jobsPage;

			const allHasSlug = checkProperty( jobs, 'slug', ctx );

			if( !allHasSlug ) return;

			const allHasApplyUrl = checkProperty( jobs, 'applyUrl', ctx );

			if( !allHasApplyUrl ) return;	

			const allHasCompany = checkProperty( jobs, 'company', ctx );

			if( !allHasCompany ) return;

			const urlObject = new URL( url );

			const searchParams = new URLSearchParams({ trigger_title: `CMS Build: ${ title || 'No title given' }` })

			urlObject.search = searchParams; 

			const res = await axios.post( urlObject.toString() ); 

			ctx.send({ 
				ok: true 
			})
		} catch ( e ){ 
			console.log( e );

			ctx.status = 500; 
		}
	},
	webhooks: async ctx => { 
		try { 
			ctx.send({ 
				ok: true, 
				webhooks: { 
					staging: process.env.NETLIFY_REBUILD_STAGING || '',
					live: process.env.NETLIFY_REBUILD_LIVE || ''
				},
				preview: process.env.PREVIEW_URL || ''
			})
		} catch ( e ) { 
			console.log( e );

			ctx.status = 500;
		}
	}, 
	netlifyWebhook: async ctx => { 
		try { 

			const { request } = ctx; 

			const { body } = request; 

			const { branch, state, title } = body; 

			console.log({ branch, state, title });

			if( !branch || !state || !title ){ 
				ctx.status = 200; 

				return { status: 200 };
			}

			if( branch === 'master' && state === 'ready' ){
				await slackGoLivePost({ 
					message: `The live site was updated successfuly.\n\n*${ title || 'No reason given' }*\n\n<https://flatworld.co|View>` 
				})
			} else if ( branch === 'master' && state === 'error' ){ 
				await slackGoLivePost({ 
					message: `The live site failed to update. Check netlify.\n\n*${ title || 'No reason given' }*\n\n<https://flatworld.co|View>` 
				})
			} else if ( branch === 'staging' && state === 'ready' ){
				await slackGoLivePost({ 
					message: `The staging site was updated successfuly.\n\n*${ title || 'No reason given' }*\n\n<${ process.env.PREVIEW_URL }|View>` 
				})
			} else if ( branch === 'staging' && state === 'error' ){
				await slackGoLivePost({ 
					message: `The staging site failed to update. Check netlify.\n\n*${ title || 'No reason given' }*\n\n<${ process.env.PREVIEW_URL }|View>` 
				})
			}			

			ctx.status = 200; 		
		} catch ( e ) { 
			console.log( e );

			ctx.status = 500;
		}
	},
};
