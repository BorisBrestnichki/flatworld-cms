'use strict';

/**
 * fill-countries.js controller
 *
 * @description: A set of functions called "actions" of the `fill-countries` plugin.
 */

module.exports = {

  /**
   * Default action.
   *
   * @return {Object}
   */

  fill: async (ctx) => {
    try { 
      const { request } = ctx;

      const { body } = request; 

      if( !body ){ 
        ctx.status = 400; 
        
        return { 
          error: 'No body'
        }
      }

      const { from, to } = body; 

      if( !from || !to ){ 
        ctx.status = 400; 

        return { 
          error: 'Specify from and to'
        }
      }

      const fromJob = await strapi.services.job.findOne({ _id: from }); 

      if( !fromJob ) { 
        ctx.status = 404; 

        return { 
          error: 'No such job'
        }
      }

      const toJob = await strapi.services.job.findOne({ _id: to }); 

      if( !toJob ) { 
        ctx.status = 404; 

        return { 
          error: 'No such job'
        }
      }

      const { countries } = fromJob; 

      await strapi.services.job.update({ _id : to }, { countries })    

      ctx.status = 200; 

      return { 
        ok: true
      }
    } catch ( e ){ 
      console.log( e );

      ctx.status = 500; 

      return { 
        error : 'Server error '
      }
    }
  }
};
