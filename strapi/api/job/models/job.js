'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
    lifecycles: {
        async beforeUpdate( query, data ) {
           await handleActiveChange( data );

            if( data.slug ){
                const { _id, slug } = data; 

                const other = await strapi.services.job.find( { _id: { $ne: _id }, slug } );

                if( other.length > 0 ){
                    throw strapi.errors.badRequest( 'There is another job with the same slug' );
                }
            }
        }
    }
};

async function handleActiveChange( data ){
    const { _id } = data; 

    if( data.activeState !== 'active' ) return; 

    const previousState = await strapi.services.job.findOne({ _id }); 

    if( previousState.activestate === data.activeState ) return; 

    const jobsPage = await strapi.services[ 'jobs-page' ].find({}); 

    if( !jobsPage ) return; 

    const { jobs } = jobsPage; 

    const jobInList = jobs.find( job => `${ job._id }` === _id ); 

    if( jobInList ) return; 

    const newJobs = [ previousState, ...jobs ];

    await strapi.services[ 'jobs-page' ].createOrUpdate({ jobs: newJobs });
}