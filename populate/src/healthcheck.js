import fetch from 'node-fetch'; 

export default async function strapiHealthcheck( counter = 0 ){ 
    if( counter > ( process.env.NODE_ENV === 'production' ? 600 : 300 )){
        return false;
    } 

    const healthy = await ping(); 

    if( !healthy ){ 
        return strapiHealthcheck( counter + 1 )
    }

    return true; 
}

async function ping(){
    try {
        await timeout( 1 * 1000 );

        const res = await fetch( 'http://strapi:1337/go-live/healthcheck' )

        if( res.status < 200 || res.status > 299 ){
            return false; 
        }

        return true; 
    } catch ( e ) { 
        return false; 
    }
}

function timeout( timeout ){
    return new Promise(( resolve, reject ) => {
        setTimeout( resolve, timeout );``
    })
}