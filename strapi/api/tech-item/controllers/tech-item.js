'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    async realmCreate( ctx ){ 
        const { request } = ctx; 

        const { body : techs } = request;

        for( const tech of techs ){ 
            const { name, uid } = tech; 

            if( !name || !uid ) return;

            const present = await strapi.services[ 'tech-item' ].findOne({ uid }); 

            if( !present ){
                await strapi.services[ 'tech-item' ].create({ uid, name })
            }
        }
    }
};
