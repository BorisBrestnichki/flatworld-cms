'use strict';

const createWithPrefill = require("../../../lib/create-with-prefill");

module.exports = {
    create: ctx => createWithPrefill({ 
        ctx, 
        service: strapi.services[ 'candidate-testemonial'], 
        model: strapi.models[ 'candidate-testemonial' ],
        prefillType: 'candidate-testemonial' 
    })
};
