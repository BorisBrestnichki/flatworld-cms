import React, { useContext } from 'react';
import { Inputs as InputsIndex } from '@buffetjs/custom';
import { useContentManagerEditViewDataManager } from 'strapi-helper-plugin';
import styled from 'styled-components';

const Wrapper = styled.div`
	& input { 
		${({ hideText }) => hideText ? 'color: transparent !important;' : '' }
	}
`

function DynamicDisabledField( props ){

	const { disabled } = props; 

	const { modifiedData } = useContentManagerEditViewDataManager();

	const { timezoneCategory } = modifiedData;

	const isOtherTimezone = timezoneCategory?.name === 'Other'; 

	return (
		<Wrapper
			hideText={ !isOtherTimezone }
		>
			<InputsIndex
				{ ...props }
				disabled={ disabled || !isOtherTimezone }
			/>
		</Wrapper>
	)
}

export default DynamicDisabledField; 