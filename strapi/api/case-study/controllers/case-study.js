'use strict';

const createWithPrefill = require("../../../lib/create-with-prefill");

module.exports = {
    create: ctx => createWithPrefill({ 
        ctx, 
        service: strapi.services[ 'case-study' ], 
        model: strapi.models[ 'case-study' ],
        prefillType: 'case_study' 
    })
};
