import React, { memo, useState, useEffect, useRef } from 'react';
import styled from 'styled-components'
import authRequest from '../../lib/auth-request';

const Wrapper = styled.div`
	padding: 1.25rem;
	min-height: 80vh;
`

const InputWrapper = styled.div`
	font-size: 16px; 
	line-height: 20px;
`

const InputLabel = styled.label`
	display: block;
`

const Input = styled.input`
	width: 100%; 
	max-width: 320px; 
	padding: 5px 20px; 
	border: 1px solid #333; 
`

const ButtonWrapper = styled.div`
	display: flex;
	align-items: center;

	margin-top: ${({ big }) => big ? '100px' : '40px' };
`;

const Button = styled.button`
	border: none;
	padding: .8rem 2rem;
	background: #333; 
	color: #fff;
	font-size: 1.6rem; 
	line-height: 1.2;

	&:hover { 
		opacity: .8; 
	}

	&[disabled] { 
		opacity: .6; 
	}
`

const ButtonMessage = styled.div`
	margin-left: 10px; 
	font-size: 16px; 
	line-height: 20px; 
	color: #333; 
`;

const Message = styled.p`
	margin: 0;
	margin-top: 2rem;
	color: #333; 
	font-size: 1.6rem; 
	line-height: 1.2;
`

const HomePage = () => {
	const [ title, setTitle ] = useState( '' );

	const [ message, setMessage ] = useState( null );

	const [ loading, setLoading ] = useState( true );

	const [ webhooks, setWebhooks ] = useState({
		staging: '', 
		live: '',
	}); 

	const [ preview, setPreview ] = useState( '' );

	const unloaded = useRef( false )

	useEffect(() => {
		getWebhooks()		

		return () => unloaded.current = true; 
	}, [])

	async function getWebhooks(){
		try  {
			const res = await authRequest({ url: '/go-live/webhooks' }); 

			if( unloaded.current ) return;

			if( !res.ok ){
				throw new Error( 'Request for webhooks not ok' );
			}

			setWebhooks( res.webhooks );
			setPreview( res.preview );
			setLoading( false );
		} catch ( e ) { 
			console.log( e ); 

			setMessage( 'Something went wrong. Please try again.' )
		}
	}

	async function onPreview( name ){
		try {
			setMessage( null );

			const url = webhooks[ name ]; 

			if( !url ) return; 

			if( !title ){ 
				setMessage( 'Please set update message' )
				return; 
			}

			setLoading( true );

			const data = await authRequest({ url: '/go-live/trigger-build', options: { 
				method: 'post',
				headers: { 
					'Content-Type': 'application/json',  
				},
				body: JSON.stringify({ 
					url, 
					title
				})
			}}); 

			if( unloaded.current ) return; 

			if( !data.ok ){
				setMessage( data.message );
				
				return; 
			}

			const message = `
				${ name === 'staging' ? 'Preview' : 'Live site' } updating. Ussualy takes 2-5 minutes. 
				${ name === 'staging' && preview ? `<a href="${ preview }" target="_blank">See here</a>` : '' }
			`

			setMessage( message );	
			
			setLoading( false )
		} catch ( e ){ 
			console.log( e ); 

			setMessage( 'Something went wrong. Please try again.' )
			setLoading( false )
		}
	}

	return (
		<Wrapper>
			<InputWrapper>
				<InputLabel
					htmlFor="update-message"
				>
					Update message
				</InputLabel>
				<Input 
					value={ title }
					onChange={ e => setTitle( e.target.value ) }
					id="update-message"
				/>
			</InputWrapper>
			<ButtonWrapper> 
				<Button 
					onClick={ () => onPreview( 'staging' ) }
					disabled={ loading || !webhooks.staging }
				>
					Deploy on Preview
				</Button>
				{ !loading && !webhooks.staging &&
					<ButtonMessage> 
						Build webhook not set
					</ButtonMessage>
				}
			</ButtonWrapper>
			<ButtonWrapper big={ true }> 
				<Button 
					onClick={ () => onPreview( 'live' ) }
					disabled={ loading || !webhooks.live }
				>
					Deploy on Live
				</Button>
				{ !loading && !webhooks.live &&
					<ButtonMessage> 
						Build webhook not set
					</ButtonMessage>
				}
			</ButtonWrapper>
			{ !message ? null :
				<Message
					dangerouslySetInnerHTML={{ __html: message }}
				/>
			}
		</Wrapper> 
	);
};

export default memo(HomePage); 
