'use strict';

const createWithPrefill = require("../../../lib/create-with-prefill");

module.exports = {
    create: ctx => createWithPrefill({ 
        ctx, 
        service: strapi.services[ 'client-success' ], 
        model: strapi.models[ 'client-success' ],
        prefillType: 'client-success' 
    })
};
