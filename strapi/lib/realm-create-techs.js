const { buildQuery } = require('strapi-utils');

module.exports = async function realmCreateTechs( company, techMetas ){
    const { roles: unformattedRoles } = company; 

    const roles = unformattedRoles || [];

    const techItems = roles.reduce(( acc, role ) => { 
        const { technicalItems : unformattedTechItems } = role; 

        const technicalItems = unformattedTechItems || {};

        const { mustHaveTech : unformattedMustHave , niceToHaveTech : unformattedNiceToHave } = technicalItems;

        const mustHaveTech = unformattedMustHave || [];
        const niceToHaveTech = unformattedNiceToHave || [];

        const allTech = [ ...mustHaveTech, ...niceToHaveTech ]; 

        return acc.concat( allTech );
    }, [] );

    const uniqueTechIds = Array.from( new Set( techItems.map( tech => tech.uid ) ) );

    const uniqueTech = uniqueTechIds.reduce(( acc, uid )  => { 
        const techMeta = techMetas.find( item => item.uid === uid )

        if( techMeta ){
            const { name } = techMeta; 

            acc.push({
                uid,
                name
            });
        }

        return acc;
    }, []);

    for( const tech of uniqueTech ){ 
        const { uid, name } = tech; 

        if( !uid || !name ) continue; 

        const present = await strapi.services[ 'tech-item' ].findOne({ uid }); 

        if( !present ){
            await strapi.services[ 'tech-item' ].create({ uid, name });
        }
    }
}