'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    async createMany( ctx ){ 
        const { request } = ctx; 

        const { body } = request; 

        for( const country of body ){
            await strapi.services[ 'country' ].create({ name: country })
        }

        return { status: 'ready' }
    }
};
